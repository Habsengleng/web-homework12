import React from 'react'

export default function PostPage({match}) {
    return (
        <div>
            <p>this is content from post {match.params.id} </p>
        </div>
    )
}
