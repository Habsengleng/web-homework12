import React from "react";
import { Card, Button } from "react-bootstrap";
import {Link} from "react-router-dom";

export default function HomePage({ data }) {
  const card = data.map((d, index) => (
    <div
      className="col-lg-3 col-md-6 col-sm-12 d-flex align-items-stretch"
      style={{ marginTop: "10px" }}
      key={index}
    >
      <Card style={{ width: "18rem" }}>
        <Card.Img variant="top" src={d.img} />
        <Card.Body>
          <Card.Title>{d.title}</Card.Title>
          <Card.Text>{d.desc}</Card.Text>
          <Button variant="primary" as={Link} to={`/posts/${index + 1}`}>
            Read More
          </Button>
        </Card.Body>
        <Card.Footer className="text-muted">uploaded 2 days ago</Card.Footer>
      </Card>
    </div>
  ));
  return <div className="row">{card}</div>;
}
