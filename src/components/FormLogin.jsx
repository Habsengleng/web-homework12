import React from "react";
import {Form,Col,InputGroup,FormControl,Button} from "react-bootstrap"
import auth from "./auth"

export default function FormLogin(props) {
  return (
    <div style={{marginTop:"20px"}}>
      <Form>
        <Form.Row className="align-items-center">
          <Col xs="auto">
            <Form.Label htmlFor="inlineFormInput" srOnly>
              Email
            </Form.Label>
            <InputGroup className="mb-2">
              <InputGroup.Prepend>
                <InputGroup.Text>Email</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl id="inlineFormInputGroup" placeholder="Input Email" />
            </InputGroup>
          </Col>
          <Col xs="auto">
            <Form.Label htmlFor="inlineFormInputGroup" srOnly>
              Password
            </Form.Label>
            <InputGroup className="mb-2">
              <InputGroup.Prepend>
                <InputGroup.Text>Password</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl id="inlineFormInputGroup" placeholder="Input password" />
            </InputGroup>
          </Col>
          <Col xs="auto">
            <Button type="button" className="mb-2" onClick={()=>{
                auth.login(()=>{
                   props.history.push("/welcome")
                })
            }}>
              Submit
            </Button>
          </Col>
        </Form.Row>
      </Form>
    </div>
  );
}
