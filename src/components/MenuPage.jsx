import React from "react";
import { Navbar, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function MenuPage() {
  return (
    <div>
      <Navbar bg="info" variant="dark" expand="lg">
        <div className="container">
          <Navbar.Brand as={Link} to="/">
            React-Bootstrap
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link as={Link} to="/home">Home</Nav.Link>
              <Nav.Link as={Link} to="/video">Video</Nav.Link>
              <Nav.Link as={Link} to="/account">Account</Nav.Link>
              <Nav.Link as={Link} to="/authen">Authentication</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </div>
      </Navbar>
    </div>
  );
}
