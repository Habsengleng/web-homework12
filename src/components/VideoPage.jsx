import React, { Component } from "react";
import { Link, Route } from "react-router-dom";

export default class VideoPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      video: [
        {
          name: "Animation",
          category: ["Action", "Romance", "Comedy"],
        },
        {
          name: "Movie",
          category: ["Adventure", "Comedy", "Crime", "Documentary"],
        },
      ],
    };
  }
  render() {
    const getCateName = ({ match }) => {
      return (
        <div>
          <h2>{match.params.cate}</h2>
        </div>
      );
    };
    const categoryPage = ({ match }) => {
      const cate = this.state.video.find(
        ({ name }) => name === match.params.name
      );
      return (
        <div>
          <h2>{cate.name} Category</h2>
          <ul>
            {cate.category.map((cates, index) => (
              <li key={index}>
                <Link to={`/video/${match.params.name}/${cates}`}>{cates}</Link>
              </li>
            ))}
          </ul>
          <Route path={"/video/:name"} exact strict render={()=><h3>Please select category</h3>}/>
          <Route path={`/video/:name/:cate`} component={getCateName} />
        </div>
      );
    };
    const vid = this.state.video.map((v, index) => (
      <li key={index}>
        <Link to={`/video/${v.name}`}>{v.name}</Link>
      </li>
    ));
    return (
      <div>
        <ul>{vid}</ul>
        <Route
          path={"/video"}
          exact
          strict
          render={() => <h2>Please Select a topic</h2>}
        />
        <Route path={`/video/:name`} component={categoryPage} />
      </div>
    );
  }
}
