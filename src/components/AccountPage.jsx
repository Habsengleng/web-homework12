import React, { Component } from "react";
import { Link } from "react-router-dom";
import queryString from "query-string";
class AccountPage extends Component {
  constructor() {
    super();
    this.state = {
      account: ["Netflix", "Zillow-Group", "Yahoo", "Modus-Create"],
    };
  }
  render() {
    const accList = this.state.account.map((acc, index) => (
      <li key={index}>
        <Link to={`/account?name=${acc}`}>{acc}</Link>
      </li>
    ));
    const getName = queryString.parse(this.props.location.search);
    return (
      <div>
        <h2>Account</h2>
        <ul>{accList}</ul>
        {/* {console.log(getName.name)} */}
        {getName.name !== undefined ? (
          <h3>The <span style={{color:"red"}}>name</span> in the query string is &quot; {getName.name} &quot;</h3>
        ) : (
          <h3>This is no name in the query String</h3>
        )}
      </div>
    );
  }
}

export default AccountPage;
