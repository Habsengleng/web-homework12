import React, { Component } from "react";
import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import MainPage from "./components/MainPage";
import MenuPage from "./components/MenuPage";
import { Container } from "react-bootstrap";
import HomePage from "./components/HomePage";
import PostPage from "./components/PostPage";
import VideoPage from "./components/VideoPage";
import AccountPage from "./components/AccountPage";
import WelcomePage from "./components/WelcomePage";
import ProtectedRoute from "./components/ProtectedRoute";
import FormLogin from "./components/FormLogin";
import NotFound from "./components/NotFound";

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      cardData: [
        {
          img:
            "https://images.cdn.circlesix.co/image/1/640/0/uploads/posts/2017/06/f7d4f50bfee2c3c429c083be5e05f952.jpg",
          title: "Card Title",
          desc:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        },
        {
          img:
            "https://pm1.narvii.com/7282/c4b02611acb5dde99c3f0d55cd6436c32845f5f7r1-1920-1080v2_uhq.jpg",
          title: "Card Title",
          desc:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ",
        },
        {
          img:
            "https://static2.cbrimages.com/wordpress/wp-content/uploads/2020/03/Tanjiro-Kamado-SM.jpg",
          title: "Card Title",
          desc:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        },
        {
          img:
            "https://cdn02.nintendo-europe.com/media/images/10_share_images/portals_3/H2x1_CharacterHub_InazumaEleven.jpg",
          title: "Card Title",
          desc:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        },
        {
          img:
            "https://scontent.fpnh2-2.fna.fbcdn.net/v/t31.0-8/10457725_936981463009374_6284123604623755847_o.jpg?_nc_cat=107&_nc_sid=dd9801&_nc_eui2=AeFrbC_jw0S76Fof_Lsi2gBcaHqv0dL9LzJoeq_R0v0vMj-4HJylLxNMHEOB3nYxNrPsl9dElkdJODyIrZEVmCX1&_nc_oc=AQmOlff1rhBsF9r9f7qlY5Ix_JfnGsETNQHmQoylm9u5q99HbbnoXDaGgSCRRiFOHic&_nc_ht=scontent.fpnh2-2.fna&oh=2d5b381524e5e1a586d31b884a0489d8&oe=5EFBDEFA",
          title: "Card Title",
          desc:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        },
      ],
    };
  }
  render() {
    return (
      <div>
        <Router>
          <MenuPage />
          <Container>
            <Switch>
              <Route path="/" exact component={MainPage} />
              <Route
                path="/home"
                render={() => <HomePage data={this.state.cardData} />}
              />
              <Route path={"/posts/:id"} component={PostPage} />
              <Route path="/video" component={VideoPage} />
              <Route path="/account" component={AccountPage} />
              <Route path="/authen" component={FormLogin} />
              <ProtectedRoute path="/welcome" component={WelcomePage} />
              <Route path="*" component={NotFound} />
            </Switch>
          </Container>
        </Router>
      </div>
    );
  }
}
